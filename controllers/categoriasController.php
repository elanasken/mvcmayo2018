<?php  
//Fichero controllers/categoriasController.php


//El controlador, tiene que llamar al modelo
// de datos, y pasar los resultados a la vista
require('models/categoriaModel.php');
require('models/categoriasModel.php');
$categorias=new Categorias();

//Recogemos la accion que queremos realizar con isset($_GET['accion'])
if(isset($_GET['accion'])){
	$accion=$_GET['accion'];
}else{
	$accion='listado';
} // Fin del if(isset($_GET['accion']))

switch($accion){
	case 'ver':
		$categoria=$categorias->dimeElemento($_GET['id']);
		echo $twig->render('categoria.html.twig', Array('categoria'=>$categoria));
		break;

	case 'insertar':
		echo $twig->render('categoriaFormulario.html.twig', Array('accion'=>'insercion'));
		break;

	case 'insercion':
		$nombre=$_POST['nombre'];
		$descripcion=$_POST['descripcion'];
		$categorias->nuevoElemento($nombre, $descripcion);
		header('location: index.php?c=categoriasController.php');
		break;

	case 'borrar':
		$id=$_GET['id'];
		$categorias->borrarElemento($id);
		header('location: index.php?c=categoriasController.php');
		break;

	case 'modificar':
		$categoria=$categorias->dimeElemento($_GET['id']);
		echo $twig->render('categoriaFormulario.html.twig', Array('categoria'=>$categoria, 'accion'=>'modificacion'));
		break;

	case 'modificacion':
	 	$nombre=$_POST['nombre'];
	 	$descripcion=$_POST['descripcion'];
	 	$id=$_POST['id'];
	 	$categorias->guardarElemento($id, $nombre, $descripcion);
	 	header('location: index.php?c=categoriasController.php');
	 	break;

	case 'listado':
	default:
		$categorias=$categorias->dimeElementos();
		echo $twig->render('categorias.html.twig', Array('categorias'=>$categorias));
		break;
} //Fin del switch($accion)
?>