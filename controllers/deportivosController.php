<?php  
//Fichero controllers/productosController.php


//El controlador, tiene que llamar al modelo
// de datos, y pasar los resultados a la vista
require('models/centroModel.php');
require('models/centrosModel.php');
$centros=new Centros();

//Como ya he traido  mi modelo de datos, extraigo los datos
// que luego le pasare a la vista

if(isset($_GET['id'])){
	//ESTE ES UN UNICO CENTRO
	$elcentro=$centros->dimeElemento($_GET['id']);
	echo $twig->render('centro.html.twig', Array('elcentro'=>$elcentro));
}else{
	$loscentros=$centros->dimeElementos();
	echo $twig->render('centros.html.twig', Array('loscentros'=>$loscentros));
}


?>