<?php  
//Fichero models/productoModel.php

class Producto{
	public $id;
	public $nombre;
	public $descripcion;
	public $precio;
	public $unidades;
	public $fecha;
	public $categoria;
	public $url;

	public function __construct($registro){
		$this->id=$registro['idProd'];
		$this->nombre=$registro['nombreProd'];
		$this->descripcion=$registro['descripcionProd'];
		$this->precio=$registro['precioProd'];
		$this->unidades=$registro['unidadesProd'];
		$this->fecha=$registro['fechaAlta'];
		$this->categoria=$registro['nombreCat'];

		$nombre=preg_replace(array('/ /'), array('-'), strtolower(trim($this->nombre)));
		$this->url='producto-'.$nombre.'-'.$this->id.'.html';
	}
} //Fin de la class Producto
?>